import { IsNumber, IsNotEmpty } from 'class-validator';

export class PriceDto {
  @IsNumber()
  @IsNotEmpty()
  price: number;

  @IsNumber()
  @IsNotEmpty()
  id: number;
}
