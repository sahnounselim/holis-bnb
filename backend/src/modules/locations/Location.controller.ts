import {
  Controller,
  Get,
  Param,
  Put,
  Body,
  Delete,
  Post,
} from '@nestjs/common';
import { LocationService } from './Location.service';
import { PriceDto } from '../dto';
import { LocationDto } from '../dto/location.dto';

@Controller('locations')
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  /** List all locations in database with this endpoint */
  @Get()
  async getLocations() {
    return await this.locationService.getLocations();
  }
  @Get(':id')
  async getOneLocation(@Param('id') id: number) {
    return await this.locationService.getOneLocation(id);
  }
  @Put('edit-price')
  async changePrice(@Body() dto: PriceDto) {
    return await this.locationService.changePrice(dto);
  }
  @Delete('remove-location')
  async deleteLocation(@Body() id: number) {
    return await this.locationService.deleteLocation(id);
  }
  @Post('add-location')
  async addLocation(@Body() location: LocationDto) {
    return await this.locationService.addLocation(location);
  }
}
