import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Location } from './Location.entity';
import { PriceDto, LocationDto } from '../dto';

@Injectable()
export class LocationService {
  constructor(
    @InjectRepository(Location)
    private readonly locationRepository: Repository<Location>,
  ) {}

  async getLocations(): Promise<Location[]> {
    const allLocations = await this.locationRepository.find();
    return allLocations;
  }
  async getOneLocation(id: number): Promise<Location> {
    const location = await this.locationRepository.findOne(id);
    return location;
  }
  async changePrice(dto: PriceDto): Promise<any> {
    const { price, id } = dto;
    await this.locationRepository.update({ id }, { price });
  }

  async deleteLocation(id: number): Promise<any> {
    await this.locationRepository.delete(id);
  }

  async addLocation(location: LocationDto): Promise<any> {
    const newLocation = await this.locationRepository.save(location);
    return newLocation;
  }
}
