import React, { useEffect, useState } from 'react';
import SearchPage from './pages/Search/Search';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Layout from './layouts/Layout';
import { getAllLocations } from './services/location.service';
import { Location } from './models';
import DisplayLocationPage from './pages/DisplayLocation/DisplayLocation';

function App() {
  const [searchInput, setSearchInput] = useState<string>('');
  const updateSearch = (arg: string): void => {
    setSearchInput(arg);
  };
  return (
    <Router>
      <Routes>
        <Route element={<Layout updateSearch={updateSearch} />}>
          <Route index element={<SearchPage searchInput={searchInput} />} />
          <Route path="/location/:id" element={<DisplayLocationPage />} />
        </Route>
      </Routes>
    </Router>
  );
}

export default App;
