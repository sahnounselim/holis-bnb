import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Location } from './../../models';
type CardProps = { locationProps: Location };
import './Card.css';
import { HiStar } from 'react-icons/hi';
const Card: React.FC<CardProps> = ({ locationProps }: CardProps) => {
  const navigate = useNavigate();
  const { id, title, description, location, picture, stars, price, categoryId, numberOfRooms } =
    locationProps;
  const pictureStyle = { background: `url(${picture}) no-repeat 50% 50%` };
  const onClick = () => {
    navigate('/location/' + id);
  };
  return (
    <div className="card" onClick={onClick}>
      <div className="card-picture" style={pictureStyle}></div>
      <div className="card-details">
        <div className="title">
          {title.length > 17 ? title.slice(0, 17) + '...' : title}, {location}
        </div>
        <div className="stars">
          {[...Array(stars)].map((x, i) => (
            <HiStar key={i} />
          ))}
        </div>
        <div className="price">
          <span>€{price} </span>night
        </div>
      </div>
    </div>
  );
};

export default Card;
