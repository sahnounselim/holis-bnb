import React, { useState } from 'react';
import { Outlet } from 'react-router-dom';
import Header from '../components/Header/Header';

type LayoutProps = {
  updateSearch: (arg: string) => void;
};

const Layout: React.FC<LayoutProps> = ({ updateSearch }) => {
  return (
    <React.Fragment>
      <Header updateSearch={updateSearch} />
      <Outlet />
    </React.Fragment>
  );
};

export default Layout;
