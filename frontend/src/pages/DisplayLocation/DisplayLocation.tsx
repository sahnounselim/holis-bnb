import React, { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { LoadingStatus } from './../../utils';
import { getLocation, changePrice, removeLocation } from '../../services/location.service';
import { Location } from './../../models';
type DisplayLocationPageProps = {};
import './DisplayLocation.css';
import { HiStar } from 'react-icons/hi';

const DisplayLocationPage: React.FC<DisplayLocationPageProps> = () => {
  const navigate = useNavigate();
  const [priceChange, setPriceChange] = useState<number>(0);
  const [updateChange, setUpdateChange] = useState<number>(0);
  const [locationDetails, setLocationDetails] = useState<Location>();
  const [loadingStatus, setLoadingStatus] = useState<LoadingStatus>(LoadingStatus.IDLE);
  const { id } = useParams();
  useEffect(() => {
    //fetch
    if (loadingStatus == LoadingStatus.SUCCESS || !id) return;
    const fetchLocation = async () => {
      setLoadingStatus(LoadingStatus.LOADING);
      getLocation(Number(id))
        .then((response) => {
          setLocationDetails(response.data);
          setLoadingStatus(LoadingStatus.SUCCESS);
        })
        .catch((e) => {
          setLoadingStatus(LoadingStatus.ERROR);
          console.error(e);
        });
    };
    fetchLocation();
  }, [updateChange]);

  // Create a function to handle price change and persist it to database
  const handlePriceChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const priceValue = e.target.value;
    setPriceChange(Number(priceValue));
  };
  const updatePrice = () => {
    if (!id || !priceChange) return;
    changePrice(priceChange, Number(id)).then(() => {
      setLoadingStatus(LoadingStatus.IDLE);
      setUpdateChange(updateChange + 1);
      setPriceChange(0);
    });
  };
  // Create a function to delete the location and persist it to database
  const deleteLocation = () => {
    removeLocation(Number(id));
    navigate('/');
  };

  if (locationDetails) {
    return (
      <div className="display-location">
        <img src={locationDetails.picture} alt="" />
        <div className="details-container">
          <div className="display-location__content">
            <span className="location-title">
              {locationDetails.title}, {locationDetails.location}
            </span>
            <div className="location-stars">
              {[...Array(locationDetails.stars)].map((x, i) => (
                <HiStar key={i} />
              ))}
            </div>
            <div className="location-rooms">
              {locationDetails.numberOfRooms} room{locationDetails.numberOfRooms > 1 ? 's' : ''}
            </div>

            <div className="location-description">{locationDetails.description}</div>
          </div>
          <div className="display-price">
            <span>€{locationDetails.price} </span>night
          </div>
          <div className="display-location__edit">
            <h2>Modify Price</h2>
            {/* price input */}
            <input
              type="number"
              min="0"
              required
              step="0.1"
              value={priceChange}
              onChange={(e) => handlePriceChange(e)}
            />
            {/* price button */}
            <div className="button-container">
              <div className="modify-button delete" onClick={deleteLocation}>
                Delete
              </div>
              {/* delete div */}
              <div className="modify-button confirm" onClick={updatePrice}>
                Confirm
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  return <div></div>;
};

export default DisplayLocationPage;
