import React, { useEffect, useState } from 'react';
import Card from '../../components/Card/Card';
import './Search.css';
import { Location } from './../../models';
import { getAllLocations } from '../../services/location.service';
import { LoadingStatus } from './../../utils';

type SearchPageProps = { searchInput: string };

const SearchPage: React.FC<SearchPageProps> = ({ searchInput }) => {
  // Create a function to fetch all locations from database
  const [loadingStatus, setLoadingStatus] = useState<LoadingStatus>(LoadingStatus.IDLE);
  const [allLocations, setAllLocations] = useState<Location[]>();
  const [filteredLocations, setFilteredLocations] = useState<Location[]>();
  const [numberOfRoomsSet, setNumberOfRoomsSet] = useState<number[]>();

  useEffect(() => {
    //fetch
    if (loadingStatus == LoadingStatus.SUCCESS) return;
    const fetchLocations = async () => {
      setLoadingStatus(LoadingStatus.LOADING);
      getAllLocations()
        .then((response) => {
          setLoadingStatus(LoadingStatus.SUCCESS);
          const sortedLocationsByrooms = response.data.sort(function (a: Location, b: Location) {
            return a.numberOfRooms - b.numberOfRooms;
          });
          setAllLocations(sortedLocationsByrooms);
          setFilteredLocations(sortedLocationsByrooms);
          let setOfRooms = Array.from(
            new Set<number>(
              sortedLocationsByrooms.map((location: Location): number => location.numberOfRooms)
            )
          );
          setNumberOfRoomsSet(setOfRooms);
        })
        .catch((e) => {
          setLoadingStatus(LoadingStatus.ERROR);
          console.error(e);
        });
    };
    fetchLocations();
  }, []);
  useEffect(() => {
    if (allLocations) {
      const filteredLocations = allLocations.filter((location) =>
        location.title.toUpperCase().includes(searchInput.toUpperCase())
      );
      !filteredLocations ? setFilteredLocations([]) : setFilteredLocations(filteredLocations);
    }
  }, [searchInput]);
  // Create a function to sort locations by categories & by number of rooms

  // Create a search function linked to the search input in the header
  if (loadingStatus == LoadingStatus.IDLE || loadingStatus == LoadingStatus.LOADING) {
    return <div></div>;
  }
  if (loadingStatus == LoadingStatus.SUCCESS && filteredLocations && numberOfRoomsSet) {
    return (
      <div className="search">
        <div className="header-title">Houses</div>
        {numberOfRoomsSet.map((rooms: number, index) => {
          return (
            <div className="location-container" key={index}>
              <div className="room-number">
                {rooms} room{rooms > 1 ? 's' : ''}
              </div>
              <div className="location-list">
                {filteredLocations.map((locationProps: Location, index) => {
                  if (locationProps.numberOfRooms === rooms) {
                    return (
                      <li key={index}>
                        <Card locationProps={locationProps} />
                      </li>
                    );
                  }
                })}
              </div>
            </div>
          );
        })}
      </div>
    );
  }
  if (loadingStatus == LoadingStatus.ERROR) {
    return <div>ERROR</div>;
  }
  return <div></div>;
};

export default SearchPage;
