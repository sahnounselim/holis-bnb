import { get, putPrice, deleteLocation } from './wrapper.service';

export const getAllLocations = async () => {
  return await get(`locations`);
};

export const getLocation = async (id: number) => {
  return await get(`locations/` + id);
};

export const changePrice = async (price: number, id: number) => {
  return await putPrice(`locations/edit-price`, { price, id });
};

export const removeLocation = async (id: number) => {
  return await deleteLocation(`locations/remove-location`, id);
};
