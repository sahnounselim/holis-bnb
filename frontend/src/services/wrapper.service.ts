import { config } from './../utils/config';
const axios = require('axios').default;
const instance = axios.create({
  baseURL: `${config.apiUrl}`,
  headers: { 'Content-type': 'application/json' }
});

export const get = async (endpoint: string) => {
  return await instance.get(`${endpoint}`);
};

export const putPrice = async (endpoint: string, payload: { price: number; id: number }) => {
  return await instance.put(`${endpoint}`, payload);
};
export const deleteLocation = async (endpoint: string, id: number) => {
  return await instance.delete(`${endpoint}`, { data: { id } });
};
